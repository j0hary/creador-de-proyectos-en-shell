#!/bin/bash

####### SECCION DE PARAMETRIZACION DEL SCRIPT  ###########
if [ "$#" -ne 5 ]
then
    echo "Argumentos insuficientes para ejecucion del script"
    echo "Modo de uso: projectcreator.sh -p nombreproyecto -f framework [-y | -n]"
    echo "Los valores validos para framework son: "
    echo "django"
    echo "laravel"
    echo "codeigniter"
    exit 1
fi
proyecto=$2
framework=$4
auto=$5
####### FIN DE SECCION DE PARAMETRIZACION   #############


#######  CODIGO PARA CREACION DE PROYECTOS DJANGO  ###########
if [ $framework == "django" ]
then
	# se verifica que este instalado Python3
	if [[ "$(python3 -V)" =~ "Python 3" ]]
	then
		echo "Python 3 esta instalado"
		#se verifica que este instalado PIP
		ispip=$(python3 -m pip --version 2>&1)
		if [[ "$ispip" =~ "No module named pip" ]]
		then
		    # si PIP no esta instalado, se procede a instalarlo
		    echo "PIP no esta instalado, se procede a instalarlo"
		    if [ $auto == "-y" ]
		    then
		    	pipoutput=$(apt-get -y install python3-pip)
		    else
		    	apt-get install python3-pip
		    fi	
			if [[ "$pipoutput" =~ "python3-pip" ]]
			then
				echo "PIP se ha instalado correctamente"
			fi
		else
		    echo "PIP esta presente en el sistema"
		fi    
		#se procede a verificar que este presente Django
		isdjango=$(python3 -m django --version 2>&1)
		if [[ "$isdjango" =~ "No module named django" ]]
		then
			#se procede a instalar Django
			echo "Django no esta instalado, se procede a instalarlo"
		    if [ $auto == "-y" ]
		    then
		    	djoutput=$(yes | python3 -m pip install Django)
		    else
		    	python3 -m pip install Django
		    fi
		    if [[ "$djoutput" =~ "Successfully installed Django" ]]
			then
				echo "Django se ha instalado correctamente"
			fi	
		else
			echo "Django esta presente en el sistema"
		fi
	else
		echo "Python no esta instalado, se procedera a instalarlo"
		
	    if [ $auto == "-y" ]
	    then
	    	py3out=$(apt-get -y install python3)
	    else
	    	apt-get -y install python3
	    fi
		#se verifica que este instalado PIP
		ispip=$(python3 -m pip --version 2>&1)
		if [[ "$ispip" =~ "No module named pip" ]]
		then
		    # si PIP no esta instalado, se procede a instalarlo
		    echo "PIP no esta instalado, se procede a instalarlo"
		    if [ $auto == "-y" ]
		    then
		    	pipoutput=$(apt-get -y install python3-pip)
		    else
		    	apt-get install python3-pip
		    fi	
			if [[ "$pipoutput" =~ "python3-pip" ]]
			then
				echo "PIP se ha instalado correctamente"
			fi
		else
		    echo "PIP esta presente en el sistema"
		fi    
		#se procede a verificar que este presente Django
		isdjango=$(python3 -m django --version 2>&1)
		if [[ "$isdjango" =~ "No module named django" ]]
		then
			#se procede a instalar Django
			echo "Django no esta instalado, se procede a instalarlo"
		    if [ $auto == "-y" ]
		    then
		    	djoutput=$(yes | python3 -m pip install Django)
		    else
		    	yes | python3 -m pip install Django
		    fi
		    if [[ "$djoutput" =~ "Successfully installed Django" ]]
			then
				echo "Django se ha instalado correctamente"
			fi	
		else
			echo "Django esta presente en el sistema"
		fi

	fi
	# se procede a la creacion del projecto
	creaproyecto=$(django-admin startproject $proyecto 2>&1)
	if [[ "$creaproyecto" =~ "CommandError" ]]
	then
		echo "Ocurrio un error al crear el proyecto, verifique que el proyecto no exista aun."
	else
		echo "Se ha creado correctamente el proyecto $proyecto"
	fi
fi
######### FIN DE CODIGO PARA DJANGO  ###########


#######  CODIGO PARA CREACION DE PROYECTOS CODEIGNITER  ###########
if [ $framework == "codeigniter" ]
then
	# se usara la version 4 del framework, por lo cual se checa que al menos PHP > 7.2
	isphp=$(apt list --installed | grep php7.2  2>&1)
	if [[ "$isphp" =~ "instalado" ]] 
	then
    	echo "PHP 7.2 esta presente"
	else
    	echo "PHP 7.2 no esta instalado, se procede a instalarlo"
    	apt-get -y install php7.2
	fi
	#se verifica que este instalado el motor de bases de datos
	ismysql=$(mysql -V)
	if [[ "$ismysql" =~ "No se ha encontrado la orden" ]]
	then
    	echo "MySQL Server no esta instalado, se procede a instalarlo"
    	apt-get -y install mysql-server
    	#verificar que el servicio este corriendo
    	systemctl start mysql-server
    	#se fija la contraseña de root del motor de bases de datos
    	mysqladmin -u root password t35tpa55w0rd
	else
    	echo "mysql-server ya esta instalado"
	fi
	# se checa que este php-curl
	iscurl=$(apt list --installed | grep php7.2-curl  2>&1)
	if [[ "$iscurl" =~ "instalado" ]] 
	then
    	echo "PHP 7.2-curl esta presente"
	else
    	echo "PHP 7.2-curl no esta instalado, se procede a instalarlo"
    	apt-get -y install php7.2-curl
	fi
	#se verifica que curl este habilitado
	iscurl=$(php -m | grep curl)
	if [[ "$iscurl" =~ "curl" ]]
	then	
		echo "curl esta habilitado"
	else
		#se habilita curl
		phpenmod curl
	fi
	# se checa que este php-curl
	isintl=$(apt list --installed | grep php7.2-intl  2>&1)
	if [[ "$isintl" =~ "instalado" ]] 
	then
    	echo "PHP 7.2-intl esta presente"
	else
    	echo "PHP 7.2-intl no esta instalado, se procede a instalarlo"
    	apt-get -y install php7.2-intl
	fi
	#se verifica que intl este habilitado
	isintl=$(php -m | grep intl)
	if [[ "$isintl" =~ "intl" ]]
	then	
		echo "intl esta habilitado"
	else
		#se habilita curl
		phpenmod intl
	fi
	# se checa que este php-curl
	isdom=$(apt list --installed | grep php7.2-dom  2>&1)
	if [[ "$isdom" =~ "instalado" ]] 
	then
    	echo "PHP 7.2-dom esta presente"
	else
    	echo "PHP 7.2-dom no esta instalado, se procede a instalarlo"
    	apt-get -y install php7.2-dom
	fi
	#se verifica que intl este habilitado
	isdom=$(php -m | grep dom)
	if [[ "$isdom" =~ "dom" ]]
	then	
		echo "dom esta habilitado"
	else
		#se habilita curl
		phpenmod dom
	fi
	# se checa que este php-mbstring
	ismbstring=$(apt list --installed | grep php7.2-mbstring  2>&1)
	if [[ "$ismbstring" =~ "instalado" ]] 
	then
    	echo "PHP 7.2-mbstring esta presente"
	else
    	echo "PHP 7.2-mbstring no esta instalado, se procede a instalarlo"
    	apt-get -y install php7.2-mbstring
	fi
	#se verifica que intl este habilitado
	ismbstring=$(php -m | grep mbstring)
	if [[ "$ismbstring" =~ "mbstring" ]]
	then	
		echo "mbstring esta habilitado"
	else
		#se habilita curl
		phpenmod mbstring
	fi	
	service apache2 restart
	#se procede a crear la base de datos del proyecto
	comando="create database $proyecto;grant all privileges on $proyecto.* to user_$proyecto@'localhost' identified by '*T35t-Pa55w0rd*';flush privileges;"
	mysql -u root -pt35tpa55w0rd -e "$comando" mysql > /dev/null
	#se crea el directorio del proyecto
	php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
	php -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
	php composer-setup.php
	rm composer-setup.php
	php composer.phar create-project codeigniter4/appstarter "$proyecto"
	#se actualiza el archivo de conexion a la base de datos
	sed -i 's/\'username\' => \'\',/\'username\' => \'algo\',/g' "$proyecto/app/Config/Database.php"
	sed -i 's/\'password\' => \'\',/\'password\' => \'Pa55w0rd\',/g' "$proyecto/app/Config/Database.php"

fi
######### FIN DE CODIGO PARA CODEIGNITER  ###########


#######  CODIGO PARA CREACION DE PROYECTOS LARAVEL  ###########
if [ $framework == "laravel" ]
then
	echo "Codigo para creacion de proyecto laravel"
fi
######### FIN DE CODIGO PARA LARAVEL  ###########